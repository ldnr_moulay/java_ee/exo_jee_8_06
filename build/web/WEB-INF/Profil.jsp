<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.time.LocalDateTime" import="java.time.format.DateTimeFormatter" %>
<!--import du "bean"-->
<%@ page import="fr.ldnr.exo_jee_7_06.modele.Profil" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <%@include file="Bandeau.jsp" %>
        <title>Profil</title>
    </head>
    <body>
        <h1>Profil</h1>
                <!-- POST au final, GET pendant le POST -->
        <form method="GET" action="modifierProfil">
            <div>Nom: <input type="text" name="nom"
                             placeholder="DOE" value="<%= Profil.getProfil().getNom() %>">
            </div>
            <div>prénom: <input type="text" name="prenom"
                                placeholder="John"
                                value="<%= Profil.getProfil().getPrenom() %>">
            </div>
            <div>Email: <input type="text" name="email"
                               placeholder="kevin.elalaoui@maroc.com"
                               value="<%= Profil.getProfil().getEmail() %>">
            </div>
            <input type="submit" value="Modifier">
        </form>
    </body>
</html>