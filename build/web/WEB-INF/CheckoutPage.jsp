<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sortie du site</title>
    </head>
    <body>
        <%
            String userName = (String) session.getAttribute("userName");
        %>
        <h3>Au revoir <%=userName%></h3>
        <p>Nous sommes heureux de vous avoir compté parmi nous
            et espérons vous revoir sur notre site très prochainement.</p>
        <br>
        <form action="LogoutServlet" method="post">
            <input type="submit" value="Déconnexion" >
        </form>
    </body>
</html>