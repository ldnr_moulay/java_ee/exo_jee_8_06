package fr.ldnr.exo_jee_7_06.controlleur;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;


/**
 * Gestion de la connexion
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    // iniformation sur le user qui peut se connecter
    private final String userID = "admin";
    private final String password = "123";
    private final String userName = "Gaston Lagaffe";

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        // get request parameters for userID and password
        String loginName = request.getParameter("login");
        String loginPwd = request.getParameter("pwd");

        // Il faudra aussi vérifier les données qui viennent de l'extérieur
        if (userID.equals(loginName) && password.equals(loginPwd)) {

            // Session , expiration au bout de 20 sec.
            HttpSession session = request.getSession();
            session.setAttribute("userName", userName);
            session.setMaxInactiveInterval(20);

            Cookie userCookie = new Cookie("userLogin", loginName);
            userCookie.setMaxAge(1 * 10);
            response.addCookie(userCookie);

            // Client side redirect
            response.sendRedirect("LoginSuccess.jsp");
        } else {

            // Server side redirect
            try (PrintWriter out = response.getWriter()) {
                RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.html");
                out.println("<font color=red>Les informations fournies sont incorrectes (essayez "
                    + userID + "/" + password + ").</font>");
                rd.include(request, response);
            }
        }

    }

}
