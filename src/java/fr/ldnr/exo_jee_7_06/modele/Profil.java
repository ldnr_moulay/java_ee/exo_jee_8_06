/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ldnr.exo_jee_7_06.modele;

/**
 *
 * @author stag
 */
public class Profil {
    private String nom = "";
    private String prenom = "";
    private String email = "";
    public static Profil profil = null;
    
    @SuppressWarnings("LeakingThisInConstructor")
    public Profil() {
        profil = this;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public static Profil getProfil() {
        if (profil == null) {
            profil = new Profil();
        }
        return profil;
    }
    
    
    
}
